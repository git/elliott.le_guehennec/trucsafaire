# TAF - multiplatform mobile to-do list app

University project by Elliott Le Guehennec

TAF stands for Trucs A Faire (things to do)

Contains a mobile app project, front and back, using React Native for the front end, and MongoDB/Java for the back end.