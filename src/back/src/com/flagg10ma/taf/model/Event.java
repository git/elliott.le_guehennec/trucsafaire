package com.flagg10ma.taf.model;

import java.time.LocalDateTime;
import java.util.Collection;

public record Event(
        String id,
        String title,
        String description,
        User creator,
        LocalDateTime startTime,
        LocalDateTime endTime,
        String colorCode,
        Collection<Task> tasks
        ) {
}
