package com.flagg10ma.taf.model;

import java.time.LocalDate;
import java.util.Collection;

public record Task(
        String id,
        String title,
        String description,
        User creator,
        LocalDate dateAssigned,
        boolean delayed,
        LocalDate dateCompleted,
        Event event,
        Collection<Label> labels
        ) {
}
