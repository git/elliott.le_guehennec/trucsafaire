package com.flagg10ma.taf.model;

public record Label(
        String id,
        String title,
        String description,
        String colorCode
) {
}
