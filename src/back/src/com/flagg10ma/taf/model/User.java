package com.flagg10ma.taf.model;

import java.util.Collection;

public record User(
        String id,
        String login,
        String passwordHash,
        Collection<Label> labels
) {
}
