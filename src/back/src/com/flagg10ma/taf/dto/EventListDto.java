package com.flagg10ma.taf.dto;

import com.flagg10ma.taf.model.Event;
import com.flagg10ma.taf.model.Label;
import com.flagg10ma.taf.model.Task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public record EventListDto(
        Collection<EventDto> events,
        Collection<LabelDto> labels
) {
    public static EventListDto fromModel(Collection<Event> events) {
        Set<Label> allLabels = new HashSet<>();

        for (Event event : events)
            for (Task task : event.tasks()) {
                allLabels.addAll(task.labels());
            }
        return new EventListDto(
                events.stream().map(EventDto::fromModel).collect(Collectors.toList()),
                allLabels.stream().map(LabelDto::fromModel).collect(Collectors.toList())
        );
    }
}
