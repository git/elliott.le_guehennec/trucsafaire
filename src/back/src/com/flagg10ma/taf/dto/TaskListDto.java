package com.flagg10ma.taf.dto;

import com.flagg10ma.taf.model.Event;
import com.flagg10ma.taf.model.Task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public record TaskListDto(
        Collection<SimplifiedTaskDto> tasks,
        Collection<SimplifiedEventDto> events,
        Collection<LabelDto> labels
) {
    public static TaskListDto fromModel(Collection<Task> tasks, Collection<Event> events) {
        Collection<SimplifiedTaskDto> taskDtos = tasks.stream()
                .map(SimplifiedTaskDto::fromModel)
                .collect(Collectors.toList());

        Collection<SimplifiedEventDto> eventDtos = events.stream()
                .map(SimplifiedEventDto::fromModel)
                .collect(Collectors.toList());

        Set<LabelDto> labelDtos = new HashSet<>();
        for (Task task: tasks) {
            labelDtos.addAll(task.labels().stream().map(LabelDto::fromModel).toList());
        }

        return new TaskListDto(taskDtos, eventDtos, labelDtos);
    }
}
