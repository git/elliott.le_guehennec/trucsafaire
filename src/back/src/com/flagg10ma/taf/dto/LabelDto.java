package com.flagg10ma.taf.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flagg10ma.taf.model.Label;

public record LabelDto(
        @JsonProperty String id,
        String title,
        String description,
        @JsonProperty("color_code") String color
) {
    public static LabelDto fromModel(Label label) {
        return new LabelDto(
                label.id(),
                label.title(),
                label.description(),
                label.colorCode()
        );
    }
}
