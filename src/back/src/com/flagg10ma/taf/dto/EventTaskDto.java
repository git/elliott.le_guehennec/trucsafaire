package com.flagg10ma.taf.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.flagg10ma.taf.model.Label;
import com.flagg10ma.taf.model.Task;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

public record EventTaskDto(
        @JsonProperty("task_id") String id,
        String title,
        String description,
        boolean delayed,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        @JsonProperty("date_assigned") LocalDate assignedDate,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        @JsonProperty("date_completed") LocalDate completionDate,

        @JsonProperty("label_ids") Collection<String> labels
) {
        public static EventTaskDto fromModel(Task task) {
                return new EventTaskDto(
                        task.id(),
                        task.title(),
                        task.description(),
                        task.delayed(),
                        task.dateAssigned(),
                        task.dateCompleted(),
                        task.labels().stream().map(Label::id).collect(Collectors.toList())
                );
        }
}
