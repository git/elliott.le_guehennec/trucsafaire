package com.flagg10ma.taf.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public record NewLabelDto(
        String title,
        String description,
        @JsonProperty("color_code") String color
) {
}
