package com.flagg10ma.taf.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public record UserDto(
        @JsonProperty String id,
        String login
){
}
