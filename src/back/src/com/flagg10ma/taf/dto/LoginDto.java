package com.flagg10ma.taf.dto;

public record LoginDto(String login, String password){}
