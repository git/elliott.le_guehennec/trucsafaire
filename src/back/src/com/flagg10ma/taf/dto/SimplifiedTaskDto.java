package com.flagg10ma.taf.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.flagg10ma.taf.model.Label;
import com.flagg10ma.taf.model.Task;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

public record SimplifiedTaskDto(
        @JsonProperty("task_id") String id,
        String title,
        String description,
        boolean delayed,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        @JsonProperty("date_assigned") LocalDate assignedDate,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        @JsonProperty("date_completed") LocalDate completionDate,

        @JsonProperty("event_id") String event,
        @JsonProperty("label_ids") Collection<String> labels
) {
        public static SimplifiedTaskDto fromModel(Task task) {
                return new SimplifiedTaskDto(
                        task.id(),
                        task.title(),
                        task.description(),
                        task.delayed(),
                        task.dateAssigned(),
                        task.dateCompleted(),
                        task.event().id(),
                        task.labels().stream().map(Label::id).collect(Collectors.toList())
                );
        }
}
