package com.flagg10ma.taf.dto;

import com.flagg10ma.taf.model.Event;
import com.flagg10ma.taf.model.Label;
import com.flagg10ma.taf.model.Task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public record CompleteEventDto(
        EventDto event,
        Collection<LabelDto> labels
) {
    public static CompleteEventDto fromModel(Event event) {
        Set<Label> allLabels = new HashSet<>();

        for (Task task : event.tasks()) {
            allLabels.addAll(task.labels());
        }
        return new CompleteEventDto(
                EventDto.fromModel(event),
                allLabels.stream().map(LabelDto::fromModel).collect(Collectors.toList())
        );
    }
}
