package com.flagg10ma.taf.dto;

public record NewUserDto(String login, String password){}
