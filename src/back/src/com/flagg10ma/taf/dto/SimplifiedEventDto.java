package com.flagg10ma.taf.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.flagg10ma.taf.model.Event;

import java.time.LocalDate;

public record SimplifiedEventDto(
        @JsonProperty("event_id") String id,
        String title,
        String description,

        @JsonProperty("start_date")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        LocalDate startDate,

        @JsonProperty("end_date")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        LocalDate endDate,

        @JsonProperty("color_code") String color,
        @JsonProperty("task_count") int taskCount
) {
        public static SimplifiedEventDto fromModel(Event event) {
                return new SimplifiedEventDto(
                        event.id(),
                        event.title(),
                        event.description(),
                        event.startTime().toLocalDate(),
                        event.endTime().toLocalDate(),
                        event.colorCode(),
                        event.tasks().size()
                );
        }
}
