package com.flagg10ma.taf.entity;

public record LabelEntity(
    String id,
    String title,
    String description,
    String colorCode
) {
}
