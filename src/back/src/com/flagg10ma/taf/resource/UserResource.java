package com.flagg10ma.taf.resource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flagg10ma.taf.model.User;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.Collections;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    //UserService userService;

    @POST
    public Response createUser(JsonNode requestBody) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String login = requestBody.get("login").asText();
            String password = requestBody.get("password").asText();

            // Create a new user
            User newUser = new User(null, login, password, Collections.emptyList());
            //userService.createUser(newUser);

            // Create the response object with createdAt and userId
            //UserResponse userResponse = new UserResponse(newUser.getId(), newUser.getCreatedAt());

            return Response.created(UriBuilder.fromPath("/api/users/{id}").build(newUser.id())).build();
        } catch (Exception e) {
            // Handle any exceptions or validation errors
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid request body").build();
        }
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response updateUser(@PathParam("id") Long id, User user) {
        // Update an existing user by ID
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteUser(@PathParam("id") Long id) {
        // Delete a user by ID
    }
}
