package com.flagg10ma.taf.resource;

import com.flagg10ma.taf.model.Task;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/tasks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TaskResource {

    @Inject
    //TaskService taskService;

    @GET
    public Response getAllTasks() {
        // Retrieve and return a list of tasks
    }

    @GET
    @Path("/{id}")
    public Response getTaskById(@PathParam("id") String id) {
        // Retrieve and return a single task by its ID
    }

    @POST
    @Transactional
    public Response createTask(Task task) {
        // Create a new task
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response updateTask(@PathParam("id") String id, Task task) {
        // Update an existing task by its ID
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteTask(@PathParam("id") Long id) {
        // Delete a task by its ID
    }
}
