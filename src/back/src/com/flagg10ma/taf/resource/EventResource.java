package com.flagg10ma.taf.resource;

import com.flagg10ma.taf.model.Event;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/events")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EventResource {

    @Inject
    //EventService eventService;

    @GET
    public Response getAllEvents() {
        // Retrieve and return a list of events
    }

    @GET
    @Path("/{id}")
    public Response getEventById(@PathParam("id") Long id) {
        // Retrieve and return a single event by its ID
    }

    @POST
    @Transactional
    public Response createEvent(Event event) {
        // Create a new event
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response updateEvent(@PathParam("id") Long id, Event event) {
        // Update an existing event by its ID
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteEvent(@PathParam("id") Long id) {
        // Delete an event by its ID
    }
}

