package com.flagg10ma.taf.resource;

import com.flagg10ma.taf.model.Label;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/labels")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LabelResource {

    @Inject
    //LabelService labelService;

    @GET
    public Response getAllLabels() {
        // Retrieve and return a list of labels
    }

    @POST
    @Transactional
    public Response createLabel(Label label) {
        // Create a new label
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response updateLabel(@PathParam("id") Long id, Label label) {
        // Update an existing label by its ID
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteLabel(@PathParam("id") Long id) {
        // Delete a label by its ID
    }
}

