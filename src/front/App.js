import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import CalendarTests from "./views/CalendarTests";

export default function App() {
  return (
    <View style={styles.container}>
      <CalendarTests style={styles.calendar}/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  calendar: {
    flex: 1,
    width: '100%',
    height: '33%',
    alignSelf: "stretch"
  }
});
