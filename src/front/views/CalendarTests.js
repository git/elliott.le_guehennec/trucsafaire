import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Calendar, LocaleConfig} from 'react-native-calendars';

export default function CalendarTests() {
    const [selected, setSelected] = useState('');

    return (
        <Calendar
            onDayPress={day => {
                setSelected(day.dateString);
            }}
            markedDates={{
                [selected]: {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'}
            }}
            style={styles.filling}
        />
    );
};

const styles = StyleSheet.create({
    filling: {
        flex: 1
    }
})